var express = require('express');
var router = express.Router();
var passport = require('passport');
var jwt = require('jsonwebtoken');
var User = require('../models/User');
var PeerConnection = require('../models/PeerConnection');
var config = require('../config');
var jwtAuth = require('../middlewares/jwt-auth');

// Register new users
router.post('/register', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({
      success: false,
      message: 'Please enter email and password.'
    });
  } else {
    var newUser = new User({
      email: req.body.email,
      password: req.body.password
    });

    // Attempt to save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({
          success: false,
          message: 'That email address already exists.'
        });
      }
      res.json({
        success: true,
        message: 'Successfully created new user.'
      });
    });
  }
});

router.get('/', jwtAuth, function(req, res) {
	User.find()
		.populate('peerConnections')
		.exec(function(err, users) {
			var fmtUsers = [];
			for(var i = 0; i < users.length; i++)
			{
				var user = users[i].toObject();
				var peerConnections = user.peerConnections;
				var activePeerConnections = peerConnections.filter(function(peerConnection)
				{
					return peerConnection.isOnline === true;
				});
				var isOnline = false;
				if(activePeerConnections.length > 0)
				{
					isOnline = true;
				}
				delete user.peerConnections;
				user.activePeerConnections = activePeerConnections;
				user.isOnline = isOnline;
				fmtUsers.push(user);
			}
			res.send({
				success: true,
				users: fmtUsers
			});
		});
});

// Authenticate the user and get a JSON Web Token to include in the header of future requests.
router.post('/auth', function(req, res) {
  User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({
        success: false,
        message: 'Authentication failed. User not found.'
      });
    } else {
      // Check if password matches
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          // Create token if the password matched and no error was thrown
          var token = jwt.sign({_id: user._id, email: user.email }, config.auth.secret, {
            expiresIn: "2 days"
          });
          res.json({
            success: true,
            message: 'Authentication successfull',
            token: token
          });
        } else {
          res.send({
            success: false,
            message: 'Authentication failed. Passwords did not match.'
          });
        }
      });
    }
  });
});

router.get('/detail', jwtAuth, function(req, res) {
	if(req.user)
	{
		res.json({
			success: true,
			user: req.user
		});
	}
	else
	{
		res.send({
			success: false,
			message: 'User could not be authenticated'
		})
	}
});

router.post('/get/peerId', jwtAuth, function(req, res) {
	PeerConnection.findOne({
		peerId: req.body.peerId
	}).populate('user')
	.exec(function(err, peerConnection)
	{
		if(err)
		{
			res.send({
	            success: false,
	            message: 'Could not find peer'
	        });
		}
		if(peerConnection)
		{
			user = peerConnection.user;
			res.send({
	            success: true,
	            user: user
	        });
		}
		else
		{
			res.send({
	            success: false,
	            message: 'Could not find peer'
	        });
		}
	})
});

// Example of required auth: protect dashboard route with JWT
router.get('/dashboard', jwtAuth, function(req, res) {
  res.send('It worked! User id is: ' + req.user.email + '.');
});


module.exports = router;
