var io = require('../../servers/socketio');
var PeerConnection = require('../../models/PeerConnection');
var SocketLib = require('../../utils/SocketLib');
io.sockets.on('connection', function (socket) {
	socket.on('storeClientInfo', function(data) {
		var socketId = socket.id;
		var customId = data.customId;
		SocketLib.storeSocketConnection(socketId, customId, io);
	});
});
module.exports = io;