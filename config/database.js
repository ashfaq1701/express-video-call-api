var app = require('./core');

var mongoose = require('mongoose');
var config = require('../config/');

mongoose.connect(config.database.local, function(err) {
	if (err) {
		console.log('Could not connect to mongodb. Ensure that you have mongodb running and mongodb accepts connections on standard ports!');
	}
});

module.exports = app;