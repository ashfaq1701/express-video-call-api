var app = require('./core');

var routes = require('../routes/index');
var users = require('../routes/users');

app.use('/', routes);
app.use('/users', users);

module.exports = app;