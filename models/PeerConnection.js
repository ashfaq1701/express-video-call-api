var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require("mongoose-relationship");

var PeerConnectionSchema = new Schema({
	peerId: {
		type: String,
		required: true
	},
	socketId: String,
	isOnline: Boolean,
	user: { 
		type: Schema.Types.ObjectId, ref:"User", childPath:"peerConnections"
	}
});

PeerConnectionSchema.plugin(relationship, { relationshipPathName:'user' });

module.exports = mongoose.model('PeerConnection', PeerConnectionSchema);