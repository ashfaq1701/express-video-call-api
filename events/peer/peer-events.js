var peerServer = require('../../servers/peer-server');
var PeerConnectionLib = require('../../utils/PeerConnectionLib');

peerServer.on('connection', function(id) {
	//console.log(id);
	PeerConnectionLib.createPeerConnection(id);
});

peerServer.on('disconnect', function(id) {
	console.log(id);
	PeerConnectionLib.updatePeerConnectionStatus(id, false);
});

module.exports = peerServer;