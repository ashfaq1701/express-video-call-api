var PeerConnection = require('../models/PeerConnection');
var User = require('../models/User');

function sendUpdateToUsers(socketId, io)
{
	PeerConnection.findOne({socketId: socketId})
		.populate('user')
		.exec(function(err, peerConnection)
		{
			if(err)
			{
				console.log(err);
			}
			if(peerConnection)
			{
				var userId = peerConnection.user._id;
				User.findById(userId)
					.populate('peerConnections')
					.exec(function(err, user)
					{
						if(err)
						{
							console.log(err);
						}
						if(user)
						{
							var currUser = user.toObject();
							var peerConnections = currUser.peerConnections;
							var activePeerConnections = peerConnections.filter(function(pConnection)
							{
								return pConnection.isOnline === true;
							});
							var isOnline = false;
							if(activePeerConnections.length > 0)
							{
								isOnline = true;
							}
							delete currUser.peerConnections;
							currUser.activePeerConnections = activePeerConnections;
							currUser.isOnline = isOnline;
							User.find()
								.populate('peerConnections')
								.exec(function(err, users)
								{
									if(err)
									{
										console.log(err);
									}
									for(var i = 0; i < users.length; i++)
									{
										var cUser = users[i];
										if(cUser.email != currUser.email)
										{
											var pConnections = cUser.peerConnections;
											for(var j = 0; j < pConnections.length; j++)
											{
												if(pConnections[j].isOnline === true)
												{
													if(pConnections[j].socketId)
													{
														io.to(pConnections[j].socketId).emit('contact', currUser);
														console.log('emitted to '+pConnections[j].socketId);
													}
												}
											}
										}
									}
								});
						}
					});
			}
		});
}

module.exports = {
	storeSocketConnection: function (socketId, peerId, io)
	{
		PeerConnection.update({peerId: peerId}, {
		    socketId: socketId
		}, function(err, numberAffected, rawResponse) {
		   if(err)
		   {
			  console.log(err); 
		   }
		   sendUpdateToUsers(socketId, io);
		});
	}
};