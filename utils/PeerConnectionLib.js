var PeerConnection = require('../models/PeerConnection');
var User = require('../models/User');

module.exports = {
	createPeerConnection: function(peerId)
	{
		var peerIdParts = peerId.split("_");
		var userId = peerIdParts[0];
		User.findById(userId, function(err, user)
		{
			if(err)
			{
				console.log(err);
			}
			if(user)
			{
				var peerConnection = new PeerConnection({
					peerId: peerId,
					isOnline: true,
					user: user._id
				});
				peerConnection.save();
			}
		});
	},
	updatePeerConnectionStatus: function(peerId, status)
	{
		PeerConnection.update({peerId: peerId}, {
		    isOnline: status
		}, function(err, numberAffected, rawResponse) {
		   if(err)
		   {
			  console.log(err); 
		   }
		});
	}
};