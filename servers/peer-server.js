var fs = require('fs');
var PeerServer = require('peer').PeerServer;
var peerServer = PeerServer({
	port: 3002, 
	path: '/',
	ssl: {
	    key: fs.readFileSync('/etc/apache2/ssl/apache.key'),
	    cert: fs.readFileSync('/etc/apache2/ssl/apache.crt')
	}
});
module.exports = peerServer;
