var app = require('../app');

var https = require('https');
var fs = require('fs');

var sslOptions = {
		key: fs.readFileSync('/etc/apache2/ssl/apache.key'),
		cert: fs.readFileSync('/etc/apache2/ssl/apache.crt')
};

var server = https.createServer(sslOptions, app);

module.exports = server;