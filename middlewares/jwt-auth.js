var passport = require('passport');

var jwtAuth = passport.authenticate('jwt', {
  session: false
});

module.exports = jwtAuth;